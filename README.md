<a href="https://cuttlefish.ch">
    <img src="src/cuttlefish_logo.svg" alt="Cuttlefish">
</a>

# Cuttlefish

Cuttlefish is a free (as in freedom) client for PeerTube, the federated video hosting service. It is Free Software as specified by the GPL, version 3 (or later).

The logo was made using a representation of a cuttlefish by [Pixelz Studio](https://thenounproject.com/term/cuttlefish/2784709/) (Distributed under the [Creative Commons CCBY license](https://creativecommons.org/licenses/by/3.0/us/legalcode)).

## What is this?

Cuttlefish is a client for PeerTube. PeerTube is a federated
video hosting service and uses WebTorrent - a version of BitTorrent that
runs in the browser - to help serve videos to users. 

Cuttlefish is a desktop client for PeerTube, but will work on GNU/Linux-based phones
(like the Librem 5 or Pinephone) as well. We want the experience of
watching PeerTube videos and using PeerTube in general to be better, by
making a native application that will become the best and most efficient
way to hook into the federation of interconnected video hosting
services. 

Eventually, it will allow people to continue sharing watched videos with
other PeerTube users for longer periods of time, instead of discarding
the video when done watching. It will also help bridge PeerTube's gap
between the - now separated - BitTorrent and WebTorrent networks by
speaking both of those protocols.

## Dependencies, protocols

We use GTK4 for the UI, with libhandy to support mobile form-factors. We will use libtorrent to speak the WebTorrent and BitTorrent protocols (since libtorrent now also supports WebTorrent). GStreamer will handle the video decoding. We use libsoup for interacting with the PeerTube API.

Cuttlefish is written in C++, but using GTK in C (instead of with gtkmm).
