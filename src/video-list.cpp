/* video-list.cpp
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libsoup/soup.h>
#include <iostream>
#include <vector>

#include <json/json.h>

#include "video-list.h"
#include "video-element.h"
#include "cuttlefish-window.h"
#include "peertube-api.h"
#include "utils.h"

struct _VideoList
{
  GtkGrid  parent_instance;

  std::string sort_method;
  std::string instance_url;
  Json::Value videos;

  GCancellable       *cancellable;

  /* Template widgets */
  GtkFlowBox          *flowbox;
  GtkStack            *stack;
  GtkViewport         *viewport;
  GtkScrolledWindow   *scrolled_window;
  GtkSpinner          *spinner;

};

G_DEFINE_TYPE (VideoList, video_list, GTK_TYPE_GRID)

typedef enum
{
  SORT_METHOD = 1,
  INSTANCE_URL,
  N_PROPERTIES
} VideoListProperty;


void video_list_open_video(VideoList* self, gint index){
  CuttlefishWindow *window = CUTTLEFISH_WINDOW(gtk_widget_get_ancestor((GtkWidget*) self, CUTTLEFISH_TYPE_WINDOW));

  window_play_video(window, self->instance_url, self->videos[index]["uuid"].asString());
}

void add_videos(VideoList* self){
  for (guint i = 0; i < self->videos.size(); ++i){
    Json::Value video = self->videos[i];

    //This concatenation would have been neater as a formatted string ("{} - {} views")
    //but that's an as of yet unimplemented C++20 feature apparently.
    std::string metadata = pretty_date(video["publishedAt"].asString());
    metadata += " - ";
    metadata += video["views"].asString();
    metadata += " views";


    GtkWidget *video_element = GTK_WIDGET (
      video_element_new (self->instance_url + video["thumbnailPath"].asCString(),
                         video["name"].asString().c_str(),
                         video["account"]["displayName"].asString(),
                         metadata,
                         i)
    );
    gtk_flow_box_insert(self->flowbox, video_element, -1);
  }
  gtk_stack_set_visible_child(self->stack, (GtkWidget*) self->scrolled_window);
}


void get_videos_cb (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data){

    VideoList *self = VIDEO_LIST (user_data);
    g_autoptr(GError) error = nullptr;

    std::string* response =
        (std::string*) g_task_propagate_pointer(G_TASK (result), &error);

    if (error)
    {
        std::cout << "Could not get video information: " << error->message << std::endl;
	if (response)
		delete response;
        return;
    }

    Json::Value root;
    std::istringstream sin(*response);
    sin >> root;
    delete response;

    g_return_if_fail (root["data"].size() > 0);
    self->videos = root["data"];

    add_videos(self);

    g_settings_apply (gsettings_get_default ());
}

void request_videos(int start, VideoList *self){
    std::string URL = self->instance_url +
                        "/api/v1/videos/?sort=-" + self->sort_method +
                        "&count=100";
    if(start != 0){
        URL += "&start=" + std::to_string(start*100);
    }

    get_response_from_url(URL, self->cancellable, get_videos_cb, self);
}


void refresh_list(VideoList* self, const std::string& instance_url){
  //First we set the spinner as the visible child, to indicate that we
  //are loading the list
  gtk_stack_set_visible_child(self->stack, GTK_WIDGET(self->spinner));

  while (auto *widget = gtk_flow_box_get_child_at_index (self->flowbox, 0)) {
    gtk_flow_box_remove (self->flowbox, GTK_WIDGET (widget));
  }

  //Make sure to update the instance url to the new one
  self->instance_url = instance_url;

  //Then we fetch the videos of this list anew.
  request_videos(0, self);
}

static void
video_list_finalize (GObject *object)
{
  VideoList *self = VIDEO_LIST (object);

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  G_OBJECT_CLASS (video_list_parent_class)->finalize (object);
}

static void
video_list_set_property (GObject       *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  VideoList *self = VIDEO_LIST (object);

  switch ((VideoListProperty) property_id)
    {
    case SORT_METHOD:
      self->sort_method = g_value_get_string (value);
      break;

    case INSTANCE_URL:
      self->instance_url = g_value_get_string (value);
      request_videos(0, self);
      break;

    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
video_list_class_init (VideoListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = video_list_set_property;
  object_class->finalize = video_list_finalize;

  GParamSpec *properties[N_PROPERTIES] = { NULL,
    g_param_spec_string ("sort-method", "sort",
                         "Sort method of the requested list",
                         NULL,
                         (GParamFlags)(G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY)),

    g_param_spec_string ("instance-url", "instance",
                         "URL of the instance",
                         NULL,
                         (GParamFlags)(G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY))
  };

  g_object_class_install_properties (object_class,
                                     N_PROPERTIES,
                                     properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/cuttlefish/app/ui/video-list.ui");
  gtk_widget_class_bind_template_child (widget_class, VideoList, stack);
  gtk_widget_class_bind_template_child (widget_class, VideoList, viewport);
  gtk_widget_class_bind_template_child (widget_class, VideoList, scrolled_window);
  gtk_widget_class_bind_template_child (widget_class, VideoList, spinner);
}

static void
video_list_init (VideoList *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->cancellable = g_cancellable_new ();

  // This probably should be moved to the UI file?
  self->flowbox = GTK_FLOW_BOX (gtk_flow_box_new ());
  gtk_flow_box_set_homogeneous (self->flowbox, TRUE);
  gtk_flow_box_set_selection_mode (self->flowbox, GTK_SELECTION_NONE);
  gtk_viewport_set_child(self->viewport, GTK_WIDGET (self->flowbox));

  // request_videos() is called after, when the instance-url property is updated
}

VideoList* video_list_new (const char *sort_method,
                           const char *instance_url)
{
  return VIDEO_LIST (
    g_object_new (VIDEO_TYPE_LIST,
                  "sort-method", sort_method,
                  "instance-url", instance_url,
                  NULL));
}
