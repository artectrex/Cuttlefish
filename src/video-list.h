/* video-list.h
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <string>

G_BEGIN_DECLS
#define VIDEO_TYPE_LIST (video_list_get_type())
G_DECLARE_FINAL_TYPE (VideoList, video_list, VIDEO, LIST, GtkGrid)
G_END_DECLS

VideoList* video_list_new (const char *sort_method,
                           const char *instance_url);

void refresh_list(VideoList* self, const std::string& instance_url);

void video_list_open_video(VideoList* self, gint index);
