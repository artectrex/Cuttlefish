/* video-view.cpp
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "video-view.h"
#include "peertube-api.h"

#include <json/json.h>

#include <future>
#include <functional>
#include <iostream>


struct _VideoView
{
  GtkBox parent_instance;

  GCancellable       *cancellable;

  // Widgets
  GtkVideo           *video;
};

G_DEFINE_TYPE (VideoView, video_view, GTK_TYPE_BOX)

void get_video_cb (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data){

    VideoView *self = VIDEO_VIEW (user_data);
    g_autoptr(GError) error = nullptr;

    std::string* response =
        (std::string*) g_task_propagate_pointer(G_TASK (result), &error);

    if (error)
    {
        std::cout << "Could not get video information: " << error->message << std::endl;
	if (response)
		delete response;
        return;
    }

    Json::Value video;
    std::istringstream sin(*response);
    sin >> video;
    delete response;

    GFile *file = g_file_new_for_uri(video["files"][0]["fileDownloadUrl"].asCString());
    gtk_video_set_file (self->video, file);
    g_object_unref(file);
}

void set_video(VideoView* self, const std::string& instance_url, const std::string& uuid){
    std::string URL = instance_url + "/api/v1/videos/" + uuid;

    get_response_from_url(URL, self->cancellable, get_video_cb, self);
}

static void
video_view_dispose (GObject *object)
{
  GtkWidget *widget = GTK_WIDGET (object);
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (widget)))
    gtk_widget_unparent (child);


  G_OBJECT_CLASS (video_view_parent_class)->dispose (object);
}

static void
video_view_finalize (GObject *object)
{
  VideoView *self = VIDEO_VIEW (object);

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  G_OBJECT_CLASS (video_view_parent_class)->finalize (object);
}


static void
video_view_class_init (VideoViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = video_view_dispose;
  object_class->finalize = video_view_finalize;

  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/cuttlefish/app/ui/video-view.ui");
  gtk_widget_class_bind_template_child (widget_class, VideoView, video);
}

static void
video_view_init (VideoView *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
