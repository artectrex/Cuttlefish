/* cuttlefish-window.h
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <string>

G_BEGIN_DECLS
#define CUTTLEFISH_TYPE_WINDOW (cuttlefish_window_get_type())
G_DECLARE_FINAL_TYPE (CuttlefishWindow, cuttlefish_window, CUTTLEFISH, WINDOW, GtkApplicationWindow)
G_END_DECLS

GSettings *gsettings_get_default ();

CuttlefishWindow* cuttlefish_window_new (GtkApplication *app);

void window_play_video(CuttlefishWindow *self,
		const std::string& instance_url,
		const std::string& video_id);
