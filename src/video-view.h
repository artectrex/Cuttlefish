/* video-view.h
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <string>

G_BEGIN_DECLS
#define VIDEO_TYPE_VIEW (video_view_get_type())
G_DECLARE_FINAL_TYPE (VideoView, video_view, VIDEO, VIEW, GtkBox)
G_END_DECLS

void set_video(VideoView* self, const std::string& instance_url, const std::string& uuid);
