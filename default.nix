{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/312a3d0b76a6025e57b6618900a3da7a8e208561.tar.gz") {} }:
# default to current master

with pkgs;
let
  libadwaita = stdenv.mkDerivation {
    name = "libadwaita";
    version = "master-2021-04-15";

    src = fetchFromGitLab {
      domain = "gitlab.gnome.org";
      owner = "GNOME";
      repo = "libadwaita";
      rev = "3695fa9af0fb14b513d1628342080dc406e5b43f";
      sha256 = "1w4dd0zcrj3r6fmbfb7xp2zyk5jr8hgyl8bz66j8nff77dbi6mnx";
    };

    mesonFlags = [ "-Dintrospection=disabled" "-Dvapi=false" ];

    buildInputs = [ gtk4 glib gobject-introspection ];
    nativeBuildInputs = [ meson ninja pkg-config sassc ];
  };
in
  stdenv.mkDerivation {
    name = "cuttlefish";

    src = ./.;

    buildInputs = [
      gtk4 gst_all_1.gstreamer libsoup glib libadwaita jsoncpp
    ];
    propagatedBuildInputs = (with gst_all_1; [
      gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly
      gst-libav ]) ++ [ glib-networking ];
    nativeBuildInputs = [
      meson ninja pkg-config desktop-file-utils
    ];
}
