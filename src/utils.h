/* utils.h
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>

#include <string>

/*
 * Takes an ugly date and returns a pretty date
 * ugly_date will be in a format like this, as returned by PeerTube:
 * "2018-08-02T13:55:13.338Z"
 *
 * The returned pretty date will be a string to show in the UI, for example:
 * "2 days ago", "just now",...
 *
 */
std::string pretty_date(const std::string& ugly_date);
