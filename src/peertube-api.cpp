/* utils.cpp
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "peertube-api.h"
#include "config.h"

#include <glib.h>
#include <gtk/gtk.h>
#include <libsoup/soup.h>

#include <filesystem>
#include <iostream>
#include <vector>


const std::filesystem::path cache_dir = std::filesystem::path(g_get_user_cache_dir()) / GETTEXT_PACKAGE;

//Check if cache_dir exists, if not make it.
const bool cache_dir_created = std::filesystem::create_directory(cache_dir);
//FIXME: this is really ugly


static GdkPaintable*
get_image_cached_blocking(gchar        *image_url_ptr,
                          GCancellable *cancellable,
                          GError       *error) noexcept(true) {

  std::string image_url = std::string(image_url_ptr);
  g_free(image_url_ptr);
  std::string file_name = image_url.substr(image_url.find_last_of("/") + 1);

  //make sure the file name isn't empty at this point
  g_return_val_if_fail(!file_name.empty(), nullptr);

  std::filesystem::path path = cache_dir / file_name;

  g_autoptr(GFile) file = g_file_new_for_path(path.c_str());
  GdkTexture *texture = gdk_texture_new_from_file (file, &error);

  if(error != NULL and error->code == G_IO_ERROR_NOT_FOUND){

    //TODO reuse session
    g_autoptr(SoupSession) session = soup_session_new();
    g_autoptr(SoupMessage) msg = soup_message_new("GET", image_url.c_str());

    g_autoptr(GInputStream) input_stream = soup_session_send(session, msg, cancellable, &error);

    GError *error_write = NULL;

    // TODO use temp file, then move instead of directly using file. CHECKME: but is it needed?
    g_autoptr(GFileOutputStream) output_stream =
        g_file_replace(file, NULL, false, G_FILE_CREATE_REPLACE_DESTINATION, cancellable, &error_write);

    //Copy the content of input_stream to output_stream by splicing
    auto bytes = g_output_stream_splice ((GOutputStream*)output_stream, input_stream,
                                         G_OUTPUT_STREAM_SPLICE_CLOSE_TARGET,
                                         cancellable,
                                         &error_write);
    if(bytes == -1){
        //Splice didn't work
        std::cout << "Got the following ERROR while tring to create a file:" << error_write->message << std::endl;
    } else {
        //reset error to NULL to ignore previous error
        g_clear_error (&error);
        //TODO try to read the message a second time instead of reading from the file?
        texture = gdk_texture_new_from_file (file, &error);
    }
  }

  if(!error){
    return (GdkPaintable*) texture;
  }

  std::cout << "Got the following ERROR while tring to open a file:" << error->message << std::endl;
  g_error_free (error);
  return NULL;
}


static void get_response_from_url_in_thread
                              (GTask        *task,
                               gpointer      source_object,
                               gpointer      task_data, //request url
                               GCancellable *cancellable) noexcept(true) {
    constexpr size_t bufsize = 4096;
    gchar *url = (gchar *) task_data;

    SoupMessage *msg = soup_message_new("GET", url);


    GError *error = NULL;

    //TODO reuse session
    g_autoptr(SoupSession) session = soup_session_new();
    g_autoptr(GInputStream) stream = soup_session_send(session, msg, cancellable, &error);

    std::string* response = new std::string;
    char buffer[bufsize];

    if(stream && !error){
        gssize bytes_read;
        do {
            bytes_read = g_input_stream_read(stream, buffer, bufsize, cancellable, &error);
            response->append(buffer, bytes_read);
        } while(bytes_read > 0 && !error);
    }

    if(error){
        g_task_return_error (task, error);
        return; //CHECKME: is this needed?
    }
    g_task_return_pointer (task, response, NULL);
}


static void
set_image_in_thread_cb        (GTask        *task,
                               gpointer      source_object,
                               gpointer      task_data, //image url
                               GCancellable *cancellable) noexcept(true)
{
      g_autoptr (GError) error = NULL;
      gchar *url = (gchar *) task_data;
      g_autoptr (GError) local_error = NULL;

      void *image = get_image_cached_blocking(url, cancellable, local_error);

      if (image) {
          g_task_return_pointer (task, image, NULL);
          return;
      }
      else if (local_error) {
          g_clear_error (&error);
          g_propagate_error (&error, local_error);
      }


    if (error)
        g_task_return_error (task, error);
    else
        g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_UNKNOWN, "Unknown error");
}




void set_image                 (const std::string&   image_url,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data) noexcept(true)
{
  gchar *url = g_strdup (image_url.c_str());

  GTask *task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_task_data (task, url, NULL);

  g_task_run_in_thread (task, set_image_in_thread_cb);
}

void get_response_from_url     (const std::string&   URL,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data) noexcept(true)
{
  gchar *url = g_strdup (URL.c_str());

  GTask *task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_task_data (task, url, NULL);

  g_task_run_in_thread (task, get_response_from_url_in_thread);
}
