/* video-element.cpp
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "video-element.h"
#include "video-list.h"

#include "peertube-api.h"
#include <iostream>
#include <future>
#include <functional>

struct _VideoElement
{
  GtkBox  parent_instance;

  GCancellable       *cancellable;

  // This is the index this element has in its VideoList
  gint                 index;

  /* Template widgets */
  GtkImage            *thumbnail;
  GtkLabel            *title_label;
  GtkLabel            *account_label;
  GtkLabel            *metadata_label;


};

G_DEFINE_TYPE (VideoElement, video_element, GTK_TYPE_BOX)

typedef enum
{
  THUMBNAIL = 1,
  TITLE,
  ACCOUNT,
  METADATA,
  INDEX,
  N_PROPERTIES
} VideoElementProperty;

static void
image_fetched_cb      (GObject      *source_object,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  VideoElement *self = VIDEO_ELEMENT (user_data);
  GError *error = NULL;

  GdkPaintable* texture = (GdkPaintable*) g_task_propagate_pointer(G_TASK (result), &error);

  if (error)
    {
    //TODO look at error and do something with it
        return;
    }

  gtk_image_set_from_paintable(GTK_IMAGE(self->thumbnail), texture);
  g_object_unref (texture);
}


static void
when_clicked (GtkGestureClick *gesture,
              int              n_press,
              double           x,
              double           y)
{
  GtkEventController *controller = GTK_EVENT_CONTROLLER (gesture);
  GtkWidget *widget = gtk_event_controller_get_widget (controller);


  VideoList *list = (VideoList*) gtk_widget_get_ancestor(widget, VIDEO_TYPE_LIST);

  VideoElement *self = (VideoElement*) widget;
  video_list_open_video(list, self->index);
}


static void
video_element_finalize (GObject *object)
{
  VideoElement *self = VIDEO_ELEMENT (object);

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  G_OBJECT_CLASS (video_element_parent_class)->finalize (object);
}


static void
video_element_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  VideoElement *self = VIDEO_ELEMENT (object);

  switch ((VideoElementProperty) property_id)
    {
    case THUMBNAIL:
      set_image(std::string(g_value_get_string (value)),
                self->cancellable,
                image_fetched_cb,
                self);
      break;

    case TITLE:
      gtk_label_set_text(self->title_label, g_value_get_string (value));
      break;

    case ACCOUNT:
      gtk_label_set_text(self->account_label, g_value_get_string (value));
      break;

    case METADATA:
      gtk_label_set_text(self->metadata_label, g_value_get_string (value));
      break;

    case INDEX:
      self->index = g_value_get_int (value);
      break;

    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}


static void
video_element_class_init (VideoElementClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);


  object_class->set_property = video_element_set_property;
  object_class->finalize = video_element_finalize;

  GParamSpec *properties[N_PROPERTIES] = { NULL,
    g_param_spec_string ("thumbnail-url", "thumbnail",
                         "URL of the video's thubmnail",
                         NULL,
                         (GParamFlags)(G_PARAM_WRITABLE|G_PARAM_CONSTRUCT)),

    g_param_spec_string ("title", "title",
                         "Title of the video",
                         NULL,
                         (GParamFlags)(G_PARAM_WRITABLE|G_PARAM_CONSTRUCT)),
    g_param_spec_string ("account", "account",
                         "Account that posted this video",
                         NULL,
                         (GParamFlags)(G_PARAM_WRITABLE|G_PARAM_CONSTRUCT)),
    g_param_spec_string ("metadata", "metadata",
                         "Time posted and views",
                         NULL,
                         (GParamFlags)(G_PARAM_WRITABLE|G_PARAM_CONSTRUCT)),
    g_param_spec_int    ("index", "index",
                         "Index this element has in its VideoList",
                         0, G_MAXINT, 0,
                         (GParamFlags)(G_PARAM_WRITABLE|G_PARAM_CONSTRUCT))
  };

  g_object_class_install_properties (object_class,
                                     N_PROPERTIES,
                                     properties);


  gtk_widget_class_set_template_from_resource (widget_class, "/ch/cuttlefish/app/ui/video-element.ui");
  gtk_widget_class_bind_template_child (widget_class, VideoElement, thumbnail);
  gtk_widget_class_bind_template_child (widget_class, VideoElement, title_label);
  gtk_widget_class_bind_template_child (widget_class, VideoElement, account_label);
  gtk_widget_class_bind_template_child (widget_class, VideoElement, metadata_label);
}

static void
video_element_init (VideoElement *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->cancellable = g_cancellable_new ();


  GtkEventController *controller = (GtkEventController*) gtk_gesture_click_new();
  g_signal_connect (controller, "pressed",
                            G_CALLBACK (when_clicked), self);
  gtk_widget_add_controller ((GtkWidget*) self, controller);
}

VideoElement*
video_element_new (const std::string& thumbnail_url,
                   const std::string& title,
                   const std::string& account,
                   const std::string& metadata,
                   int                index)
{
  return VIDEO_ELEMENT (
      g_object_new (VIDEO_TYPE_ELEMENT,
                    "thumbnail-url", thumbnail_url.c_str(),
                    "title", title.c_str(),
                    "account", account.c_str(),
                    "metadata", metadata.c_str(),
                    "index", index,
                    NULL));
}
