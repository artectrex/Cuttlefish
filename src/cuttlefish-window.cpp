/* cuttlefish-window.cpp
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "cuttlefish-window.h"
#include <adwaita.h>

#include "video-list.h"
#include "video-view.h"
#include "popover_menu/instance-dialog.h"

#include <iostream>
#include <array>

enum
{
  TITLE = 0,
  ICON,
  SORT_METHOD,
  N_LISTS
};

std::array<std::array<std::string, N_LISTS>, N_LISTS> values = {{
            {"Recently Added", "appointment-new-symbolic", "publishedAt"},
            {"Most Viewed", "face-devilish-symbolic", "views" },
            {"Trending", "starred-symbolic", "trending" }
        }};

struct _CuttlefishWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  AdwHeaderBar        *header_bar;
  GtkStack            *scroll_stack;
  GtkStack            *video_list_stack;
  AdwViewSwitcherBar  *switcher_bar;
  GtkLabel            *title_label;
  AdwSqueezer         *squeezer;
  GtkButton           *back_button;

};

G_DEFINE_TYPE (CuttlefishWindow, cuttlefish_window, GTK_TYPE_APPLICATION_WINDOW)


GSettings *gsettings_get_default ()
{
  static GSettings *settings = g_settings_new("ch.cuttlefish.app");
  return settings;
}

static void
back_button_clicked(GtkButton *button, CuttlefishWindow *self)
{
  gtk_widget_hide ((GtkWidget*) self->back_button);
  GtkWidget *view = gtk_stack_get_child_by_name (self->scroll_stack, "video-view");
  gtk_stack_remove (self->scroll_stack, view);

  gtk_stack_set_visible_child(self->scroll_stack, GTK_WIDGET(self->video_list_stack));

  gtk_widget_show ((GtkWidget*) self->switcher_bar);
  gtk_widget_show ((GtkWidget*) self->squeezer);

}

void refresh_lists(GtkStack *video_list_stack, std::string instance_url){
  for(const auto value: values){
    VideoList *list = (VideoList*) gtk_stack_get_child_by_name (video_list_stack,
                             value[SORT_METHOD].c_str());
    refresh_list(list, instance_url);
  }
}

void window_play_video(CuttlefishWindow *self, const std::string& instance_url, const std::string& video_uuid){
    GtkWidget *view   = (GtkWidget*) g_object_new (VIDEO_TYPE_VIEW, NULL);

    set_video(VIDEO_VIEW(view), instance_url, video_uuid);

    gtk_stack_add_named(self->scroll_stack, view, "video-view");

    gtk_widget_hide ((GtkWidget*) self->switcher_bar);
    gtk_widget_hide ((GtkWidget*) self->squeezer);

    gtk_stack_set_visible_child(self->scroll_stack, view);

    gtk_widget_show ((GtkWidget*) self->back_button);
}

static void
on_child_changed (GtkWidget        *widget,
                  GParamSpec       *pspec,
                  CuttlefishWindow *self)
{
  GtkWidget *current_shown_squeezer = adw_squeezer_get_visible_child(self->squeezer);

  //We want to show the bottom bar if the squeezer's currently visible child is
  //the title label ("Cuttlefish" text)
  bool show_bottom_bar = current_shown_squeezer == (GtkWidget*) self->title_label;
  adw_view_switcher_bar_set_reveal(self->switcher_bar, show_bottom_bar);


}

static void
about_dialog (GSimpleAction* action, GVariant* variant, gpointer data)
{
    GdkPaintable *logo = (GdkPaintable*) gdk_texture_new_from_resource ("/ch/cuttlefish/app/cuttlefish_logo.svg");
    const char *authors[] = {"Matthieu De Beule", "Antoine Fontaine", "Andrew Dobis", NULL};
    gtk_show_about_dialog (GTK_WINDOW(data),
                               "logo", logo,
                               "version", PACKAGE_VERSION,
                               "comments", "Client for PeerTube, the federated video hosting service.",
                               "authors", authors,
                               "website", "https://cuttlefish.ch",
                               "license-type", GTK_LICENSE_GPL_3_0,
                               NULL);
}

static void
change_instance_cb(InstanceDialog *dialog, gint response_id, CuttlefishWindow *self){

    switch (response_id)
    {
    case GTK_RESPONSE_OK: {
        //TODO validate url, error messages
        auto instance_url = instance_entry_text(dialog);
        // don't apply the change right of the gsetting right now,
        // wait for it to proove itself worthy.
        g_settings_delay (gsettings_get_default ());
        g_settings_set_string(gsettings_get_default (),
                              "instance-url",
                              instance_url.c_str());
        refresh_lists(self->video_list_stack, instance_url);
       break;
    }
    default:
       break;
    }


    gtk_window_destroy(GTK_WINDOW(dialog));

}

static void
change_instance (GSimpleAction* action, GVariant* variant, gpointer data)
{
 // Create the widget
 InstanceDialog *dialog = (InstanceDialog*) g_object_new(INSTANCE_TYPE_DIALOG, NULL);
 gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(data));


 // Connect the dialog response to callback

 g_signal_connect(dialog,
                  "response",
                  G_CALLBACK (change_instance_cb),
                  data);

 gtk_widget_show (GTK_WIDGET(dialog));
}


static void
cuttlefish_window_class_init (CuttlefishWindowClass *klass)
{

  adw_init();
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/ch/cuttlefish/app/ui/cuttlefish-window.ui");
  gtk_widget_class_bind_template_child (widget_class, CuttlefishWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, CuttlefishWindow, video_list_stack);

  gtk_widget_class_bind_template_child (widget_class, CuttlefishWindow, switcher_bar);
  gtk_widget_class_bind_template_child (widget_class, CuttlefishWindow, squeezer);
  gtk_widget_class_bind_template_child (widget_class, CuttlefishWindow, title_label);
  gtk_widget_class_bind_template_child (widget_class, CuttlefishWindow, scroll_stack);
  gtk_widget_class_bind_template_child (widget_class, CuttlefishWindow, back_button);

  gtk_widget_class_bind_template_callback(widget_class, on_child_changed);
  gtk_widget_class_bind_template_callback(widget_class, back_button_clicked);
}

static void
cuttlefish_window_init (CuttlefishWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  GActionEntry entries[] = {
    { "change_instance", change_instance, nullptr, nullptr, nullptr, {} },
    { "about", about_dialog, nullptr, nullptr, nullptr, {} },
  };

  GSimpleActionGroup *group = g_simple_action_group_new();
  g_action_map_add_action_entries (G_ACTION_MAP (group), entries, G_N_ELEMENTS (entries), self);

  gtk_widget_insert_action_group (GTK_WIDGET (self), "win", G_ACTION_GROUP(group));

  for(const auto value: values){

    const gchar *instance_url = g_settings_get_string(
            gsettings_get_default (), "instance-url");
    GtkWidget *list = GTK_WIDGET (
        video_list_new (value[SORT_METHOD].c_str(), instance_url));
    gtk_stack_add_titled(self->video_list_stack, list, value[SORT_METHOD].c_str(), value[TITLE].c_str());

    GtkStackPage *page = gtk_stack_get_page (self->video_list_stack, list);

    //TODO check why icons are not working. List of icons in GTK4? (Note: they do work when installing the flatpak on the system for some reason?)
    g_object_set(G_OBJECT(page), "icon-name", value[ICON].c_str(), NULL);

  }
}

CuttlefishWindow* cuttlefish_window_new (GtkApplication *app)
{
  return CUTTLEFISH_WINDOW (
      g_object_new (CUTTLEFISH_TYPE_WINDOW,
                    "application", app,
                    "default-width", 600,
                    "default-height", 300,
                    NULL));
}
