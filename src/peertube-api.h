/* utils.h
 *
 * Copyright 2020 Matthieu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>
#include <gio/gio.h>

#include <string>

/*
 * Set the image from the given URL, blocking. This function will
 * try to load the image from cache if it is there.
 * TODO fix description
 * Since this is blocking, do not call from the main thread
 * (so you should probably use std::async for this)
 *
 */
void set_image                 (const std::string&   image_url,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data) noexcept(true);


void get_response_from_url     (const std::string&   URL,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data) noexcept(true);
